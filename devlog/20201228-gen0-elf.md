# 2020-12-28: ELF

[2020-12-27 に作った][20201227-gen0-elf] ELF ファイルだが、 `PT_PHDR` のヘッダが不要との指摘を受け、実際に削除してみたところ問題なく動いた。

どうもこれは実行時にメモリ上に program header table を乗せて解釈しなければならない用途でのみ必要になるっぽい、たとえば動的リンクとか。
詳しくわからないが。

> you'd generally use PT_PHDR and loadable program headers if you needed to find them again at runtime (if, for eg, you were dynamic).
>
> ——[Bug #3840: executables with PT_PHDR program headers should not necessarily require PT_INTERP - illumos gate - illumos][illumos-org-issues-3840]

とりあえず不要ということはわかったので必要になるまでは省いていこう。

[20201227-gen0-elf]: 20201227-gen0-elf.md
[illumos-org-issues-3840]: https://www.illumos.org/issues/3840
