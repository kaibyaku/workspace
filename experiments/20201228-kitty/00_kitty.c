///bin/true<<//
/*
//
gcc -std=c99 "$0" -o ./kitty-c.bin && ./kitty-c.bin
exit
*/

#include <unistd.h>
#include <stdint.h>

#define BUF_SIZE 1024

void exit(int rc);
int64_t write_all(const uint8_t *buf, uint64_t rest_size);

int main(void) {
	uint8_t buf[BUF_SIZE];

	while(1) {
		const int64_t read_size = read(STDIN_FILENO, buf, BUF_SIZE);
		if(read_size < 0) {
			// Read error.
			exit(1);
		}
		if(read_size == 0) {
			// No more bytes to read or write.
			break;
		}

		const int64_t write_result = write_all(buf, read_size);
		// `read_size` is unused now.
		if(write_result < 0) {
			// Write error.
			exit(2);
		}
	}

	fsync(STDOUT_FILENO);
	close(STDOUT_FILENO);
	exit(0);
}

int64_t write_all(const uint8_t *buf, uint64_t rest_size) {
	while(1) {
		if(rest_size == 0) {
			return 0;
		}
		const int64_t written_size = write(STDOUT_FILENO, buf, rest_size);
		if(written_size < 0) {
			// Write error.
			return written_size;
		}
		buf += written_size;
		rest_size -= written_size;
	}
}

void exit(int rc) {
	_exit(rc);
}
