#!/bin/sh
set -eu

cd "$(dirname "$(readlink -f "$0")")"

TEST_STRING=foo$'\n'bar$'\n'baz$'\n'quux

fail_with_message() {
	EXIT_CODE="${1:-1}"
	NAME="${2:-"(unknown)"}"
	if [ "$1" -ne 0 ] ; then
		echo "Test failed: the binary ${NAME} should exit with zero." >&2
		exit "$EXIT_CODE"
	fi
}

gcc -std=c99 00_kitty.c -o kitty-c.bin
OUT_STRING="$( echo "$TEST_STRING" | ./kitty-c.bin)"
if [ "$OUT_STRING" != "$TEST_STRING" ] ; then
	fail_with_message 1 "kitty-c.bin"
fi

nasm 10_kitty.asm -felf64 -o kitty-asm.o
ld kitty-asm.o -o kitty-asm.bin
OUT_STRING="$( echo "$TEST_STRING" | ./kitty-asm.bin)"
if [ "$OUT_STRING" != "$TEST_STRING" ] ; then
	fail_with_message 2 "kitty-asm.bin"
fi
