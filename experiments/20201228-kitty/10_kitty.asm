; compile: nasm 10_kitty.asm -o kitty-asm.o && ld kitty-asm.o -o kitty-asm.bin

BITS 64

global _start

section .text

;#define BUF_SIZE 1024
%define BUF_SIZE 1024
;
;void exit(int rc);
;int64_t write_all(const uint8_t *buf, uint64_t rest_size);
;
;int main(void) {
_start:
;	uint8_t buf[BUF_SIZE];
	sub rsp, BUF_SIZE
;
;	while(1) {
.loop_start_read_write:
;		const int64_t read_size = read(STDIN_FILENO, buf, BUF_SIZE);
	; read: system call number 0.
	xor eax, eax
	; STDIN_FILENO == 0.
	xor edi, edi
	; buf == rsp
	mov rsi, rsp
	; BUF_SIZE
	mov rdx, BUF_SIZE
	; syscall
	syscall
;		if(read_size < 0) {
	test rax, rax
	jns .read_success
;			// Read error.
;			exit(1);
	xor edi, edi
	inc edi
	jmp exit
;		}
.read_success:
;		if(read_size == 0) {
;			// No more bytes to read or write.
;			break;
;		}
	; Flags are still unchanged since `test rax, rax`.
	jz .loop_end_read_write
.read_nonempty:
;
;		const int64_t write_result = write_all(buf, read_size);
	mov rdi, rsp
	mov rsi, rax
	call write_all
	; Now rax is `write_result`.
;		// `read_size` is unused now.
;		if(write_result < 0) {
	test rax, rax
	jns .loop_start_read_write
;			// Write error.
;			exit(2);
	xor edi, edi
	inc edi
	inc edi
	jmp exit
;		}
;	}
	; Should never reach here.
.loop_end_read_write:
;
;	fsync(STDOUT_FILENO);
	; edi = STDOUT_FILENO (== 1).
	xor edi, edi
	inc edi
	mov eax, 0x4a
	syscall
;	close(STDOUT_FILENO);
	; Still rdi == 1.
	mov eax, 0x03
	syscall
;	exit(0);
	xor edi, edi
	jmp exit
;}
;
;int64_t write_all(const uint8_t *buf, uint64_t rest_size) {
write_all:
	; NOTE: Don't use rcx and r11, since they will be destroyed by syscall.
	; rdi is `buf`, rsi is `rest_size`.
	mov rdx, rsi
	mov rsi, rdi
	; Now rsi is `buf`, rdx is `rest_size`.
;	while(1) {
	test rdx, rdx
.loop_start_write:
	; NOTE: Flag should be updated for rdx at the end of the loop.
;		if(rest_size == 0) {
	jnz .buf_is_nonempty
;			return 0;
	xor eax, eax
	ret
;		}
.buf_is_nonempty:
;		const int64_t written_size = write(STDOUT_FILENO, buf, rest_size);
	; write: system call number 1.
	xor eax, eax
	inc eax
	; STDOUT_FILENO == 1.
	xor edi, edi
	inc edi
	; buf is already stored in rsi, and won't be destroyed by syscall.
	; rest_size is already stored in rdx, and won't be destroyed by syscall.
	; syscall
	syscall
;		if(written_size < 0) {
	test rax, rax
	jns .write_success
;			// Write error.
;			return written_size;
	ret
;		}
.write_success:
;		buf += written_size;
	add rsi, rax
;		rest_size -= written_size;
	sub rdx, rax
;	}
	; NOTE: Flag should be updated for rdx at the end of the loop.
	jmp .loop_start_write
.loop_end_write:
;}
;
;void exit(int rc) {
exit:
;	_exit(rc);
	mov eax, 60
	syscall
;}
