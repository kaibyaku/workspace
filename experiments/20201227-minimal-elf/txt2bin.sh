#!/bin/sh

printf "$(cat "$@" | grep -v '^\s*#' | tr -d '[:space:]' | tr '?' 'F' | sed -e 's/../\\x&/g')"
