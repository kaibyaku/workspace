#!/bin/sh
set -eu

cd "$(dirname "$(readlink -f "$0")")"

./txt2bin.sh *.txt >exit-with-zero.bin
chmod +x exit-with-zero.bin
./exit-with-zero.bin
RET=$?
if [ $RET -ne 0 ] ; then
	echo "Test failed: exit-with-zero.bin should exit with zero." >&2
	exit $RET
fi
