# ELF (Executable and Linkable Format)

## References
* [ELF - OSDev Wiki][wiki-osdev-org:elf]
* [Executable and Linkable Format - Wikipedia][wikipedia:elf]
* [Tips　ELFフォーマットその１　ELFフォーマットについて][softwaretechnique-jp:elf-format]
* [Tips　ELFフォーマットその２　CPU依存部分について（IntelのCPU）][softwaretechnique-jp:elf-cpu-dependent]
* [Tips　ELFフォーマットその３　OS依存部分について（UNIX System V Release4）][softwaretechnique-jp:elf-os-dependent]
* [最小限のELF | κeenのHappy Hacκing Blog][keens-github-io-elf-minimal]
* [ELFに蔓延るNULL三姉妹 - Speaker Deck][speakerdeck-drumato-elf-null]
* [elf(5) - Linux man page][man-5:elf]
    + [Man page of ELF][man-ja-5:elf] (和訳)
* [ELF実行ファイルのメモリ配置はどのように決まるのか - ももいろテクノロジー][inaz2-hatenablog-com:elf-executable-memory]
* System call
    + [linux-v5.10/arch/x86/entry/syscalls/syscall\_64.tbl][src:linux:5.10:syscall64]
    + [Syscall Number for x86-64 linux (A)][mztn-org-xlasm64:syscall-numbers]
    + [Assembly Programming on x86-64 Linux (01)][mztn-org-xlasm64-amd01]

[inaz2-hatenablog-com:elf-executable-memory]: http://inaz2.hatenablog.com/entry/2014/07/27/205913
[keens-github-io-elf-minimal]: https://keens.github.io/blog/2020/04/12/saishougennoelf/
[man-5:elf]: https://linux.die.net/man/5/elf
[man-ja-5:elf]: https://linuxjm.osdn.jp/html/LDP_man-pages/man5/elf.5.html
[mztn-org-xlasm64-amd01]: https://www.mztn.org/lxasm64/amd01.html
[mztn-org-xlasm64:syscall-numbers]: https://www.mztn.org/lxasm64/x86_x64_table.html
[softwaretechnique-jp:elf-cpu-dependent]: http://softwaretechnique.jp/OS_Development/Tips/ELF/elf02.html
[softwaretechnique-jp:elf-format]: http://softwaretechnique.jp/OS_Development/Tips/ELF/elf01.html
[softwaretechnique-jp:elf-os-dependent]: http://softwaretechnique.jp/OS_Development/Tips/ELF/elf03.html
[speakerdeck-drumato-elf-null]: https://speakerdeck.com/drumato/elfniman-yan-runullsan-zi-mei
[src:linux:5.10:syscall64]: https://github.com/torvalds/linux/blob/v5.10/arch/x86/entry/syscalls/syscall_64.tbl
[wiki-osdev-org:elf]: https://wiki.osdev.org/ELF
[wikipedia:elf]: https://en.wikipedia.org/w/index.php?title=Executable_and_Linkable_Format&oldid=992446183
